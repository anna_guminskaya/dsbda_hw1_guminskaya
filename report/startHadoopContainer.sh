#!/usr/bin/env bash
if [[ $# -eq 0 ]] ; then
    echo 'You should specify docker container name!'
    exit 1
fi

echo "Creating .jar file..."
mvn package -f ../pom.xml

docker start $1
echo "Waiting for container starting..."

sleep 20

echo "Generating input data: "

./generateInputData.sh input

docker cp result/. $1:/tmp/result
docker cp ../target/homework1-1.0-SNAPSHOT-jar-with-dependencies.jar $1:/tmp
docker cp startInHDFS.sh $1:/

rm -rf result
echo "Go to container with 'docker exec -it $1 bash' command"