==========ССЫЛКА НА bb============
https://bitbucket.org/anna_guminskaya/dsbda_hw1_guminskaya/src

===========ЗАПУСК MapReduceApplication ==========

1. docker pull zoltannz/hadoop-ubuntu:2.8.1
2. docker run --name hadoop -p 2122:2122 -p 8020:8020 -p 8030:8030 -p 8040:8040 -p 8042:8042 -p  8088:8088 -p 9000:9000 -p 10020:10020 -p 19888:19888 -p 49707:49707 -p 50010:50010 -p 50020:50020 -p 50070:50070 -p 50075:50075 -p 50090:50090 zoltannz/hadoop-ubuntu:2.8.1 /etc/bootstrap.sh -bash
3. В папке homework1/report запустить скрипт ./startHadoopContainer container_name
4. По завершении скрипта испольнить команду (она написана в скрипте) docker exec -it bash container_name
5. В контейнере запустить скрипт ./startInHDFS.sh

=================================================
В папке report так же представлены

1. Скриншот использования счётчиков - Counters Usage.png
2. Скриншот успешно выполненных тестов - Successful Tests.png
3. Скриншот успешно выполненной джобы - Successful Job.png
4. Скриншот успешной загрузки файла в HDFS - Successful HDFS.png
5. Скриншот результатов работы (содержимого CSV файла) - Job Result.png
