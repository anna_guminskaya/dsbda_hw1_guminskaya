#!/usr/bin/env bash

echo "Welcome to docker container!"

cd $HADOOP_PREFIX
bin/hadoop dfs -rm -r /hw1
bin/hadoop fs -mkdir /hw1
bin/hadoop fs -put /tmp/result/ /hw1/input
bin/hadoop jar /tmp/homework1-1.0-SNAPSHOT-jar-with-dependencies.jar /hw1/input /hw1/output